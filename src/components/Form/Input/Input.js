import React from 'react'
import { useState, useEffect } from "react";
import axios from "axios";
import { Controller } from "react-hook-form";
import ReactSelect from "react-select";

const Input = ({ control, data, onChange }) => {
    // const [, setUniverse] = useState([]);
    
    const newData = () => {
        const arr = data.map(el => 
            ({ value: el.uni, label: el.uni})
        );
        return arr
    }
    
    return (
        <>
            <Controller
            name="ReactSelect"
            control={control}
            render={({ field }) => (
                <ReactSelect 
                isClearable
                    {...field}
                      options={newData()}
                      onChange={(e) => onChange(e)}
                />
                )}
            />
        </>
            
    )
}

export default Input;