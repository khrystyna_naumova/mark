import { useForm} from "react-hook-form";
import { useState, useEffect } from "react";
import axios from "axios";
import './Form.css'
import Input from './Input/Input';
import SelectField from "./SelectField/SelectField";
import { Store } from 'react-notifications-component';
import 'react-notifications-component/dist/theme.css';
import { useQuery, gql, useMutation } from '@apollo/client';

const UNIVERSE_QUERY = gql`
    {
      entries (section: "institutions", orderBy: "title DESC") {
        id
        specialty
        state
        PGY
      }
    }
`

const SUBMIT_DATA = gql`
mutation PostMutation(
    $specialties: String!
    $states: String!
    $PGY: String!
    $universe: String!
  ) {
    post(specialties: $specialties, states: $states, PGY: $PGY, universe: $universe) {
      id
      specialties
      states
      PGY
      universe
    }
  }
`;

const Form = () => {
    const { handleSubmit, control } = useForm();
    const [specialties, setSpecialties] = useState('');
    const [states, setStates] = useState('');
    const [PGY, setPGY] = useState('');
    const [informations, setInformations] = useState([]);
    const [universe, setUniverse] = useState([]);
    const [selectedUniverse, setSelectedUniverse] = useState('')

    const { data } = useQuery(UNIVERSE_QUERY); //gql query
    const [sentData, setSentData] = useState({ specialties:'', states:'', PGY:'', selectedUniverse:''});
    const [postData] = useMutation(SUBMIT_DATA, {
        variables: {
          specialty: sentData.specialties,
          state: sentData.states,
          PGY: sentData.PGY,
          universe: sentData.selectedUniverse
        }
    });

    useEffect(() => { fetchInfo() }, []);
    
    async function fetchInfo() {
        try {
            const response = await axios.get("https://63d121b43f08e4a8ff913936.mockapi.io/user")
            setInformations(response.data)
        } catch (e) {} 
    }
 
    async function getUniverse() {
        try {
            const response = await axios.get("https://63d12ec1d5f0fa7fbdc5db2b.mockapi.io/Uni")
            setUniverse(response.data)
        } catch (e) {} 
    }

    useEffect(() => {
        if (specialties !== '' && states !== '' && PGY !== '') {
            getUniverse();
        }
    }, [specialties, states, PGY]);

   
      const onPopUp = (type, title, message) => {
        Store.addNotification({
            title: title,
            message: message,
            type: type,
            insert: "top",
            container: "top-right",
            showIcon: true,
            animationIn: ["animated", "zoomIn"],
            animationOut: ["animated", "zoomOut"],
            width: 200,
            dismiss: {
              duration: 1000,
              onScreen: true
            }
          });
      }

      const onSubmitHandler = () => {
        if (specialties !== '' && states !== '' && PGY !== '' && selectedUniverse !== '') {
            onPopUp("success", "Success", "Your form has been submitted successfully");
        }else(
            onPopUp("danger", "Error", "Please fill all fields")
        )
      }

      const onSubmit = () => {
        // postData() // gql query
        console.log({ specialties, states, PGY, selectedUniverse});
      }
     
      
    return (
        <div className='form__container'>
            <form id='form__main' onSubmit={handleSubmit(onSubmit)} className="form">
                <h2 className='form__header'>Specify Your Current Program</h2>
                <div className='form__inputs'>
                    <div className='form__block'>
                        <div className='select__title'>Specialty:</div>
                        <SelectField control={control} name={'specialties'} data={informations}  onChange={setSpecialties}/>
                    </div>
                    <div className='form__block'>
                        <div className='select__title1'>State:</div>
                        <SelectField control={control} name={'states'} data={informations} onChange={setStates}/>
                    </div>
                    <div className='form__block'>
                        <div className='select__title2'>PGY:</div>
                        <SelectField control={control} name={'PGY'} data={informations} onChange={setPGY}/>
                    </div>
                    <p>Institution Name (omit, or include to limit selections):</p>
                    <Input control={control} name={'universe'} data={universe} onChange={(e) => {e ? setSelectedUniverse(e.value) : setSelectedUniverse('')}} />

                    <button onClick={() => onSubmitHandler()} className="form__btn"type="submit">Procced</button>
                </div>
            </form>
        </div>
    )
}

export default Form;