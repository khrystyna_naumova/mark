import { Controller } from "react-hook-form";
import { Select} from "antd";


const SelectField = ({ control, name, data, onChange }) => {
  const { Option } = Select;
  return (
    <Controller
      control={control}
      name={name}
      render={({ field }) => (
        <Select {...field} defaultValue="" style={{ width: '500px' }} onChange={(e)=> onChange(e, name)} >
          <Option value=''>Choose your option</Option>
            {data.map((PGY) => (
                <Option value={PGY.PGY} key={PGY.PGY} >{PGY.PGY}</Option>
          ))}
          </Select>
        )}
        />
    )
}

export default SelectField;