import { ReactNotifications } from 'react-notifications-component'

import Form from './components/Form/Form';

const App = () => {
  return (
    <>
    <ReactNotifications />
      <Form/>
    </>
  );
}
 
export default App;
